from random import randint
name = input("Hi! What is your name?")

# Guess 1

month_number = randint(1, 12)
if month_number == 2:
    day_number = randint(1, 28)
elif month_number == 1 \
    or month_number == 3 \
    or month_number == 5 \
    or month_number == 7 \
    or month_number == 8 \
    or month_number == 10 \
    or month_number == 12:
    day_number = randint(1, 31)
else:
    day_number = randint(1, 30)
year_number = randint(1924, 2004)

print("Guess 1:", name, "were you born on", str(month_number) + "/" + str(day_number) + "/" + str(year_number) + "?")


response = input("Yes or no?")
if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")


month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 2 :", name, "were you born in", month_number, "/", year_number, "?")

if response == "yes":
    print("I knew it!")
    exit()
